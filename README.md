# Boostrap Sass with VSC Live Sass Compiler

## Getting started

- [ ] git init
- [ ] git clone "https://url_de_ce_repository"

Vous pouvez télécharger la dernière version >> SOURCES FILES << de Bootstrap afin d'avoir le dossier SCSS et JS à jour.
La version utilisée ici est la V5.2.3 le 29/03/2023.

- [ ] Installer la dernière version de l'extension Visual Studio Code "Live Sass Compiler".
      Pour utiliser l'extension, il suffit de cliquer sur "Watch Sass" dans la tab bar en bas à droite de la fenêtre de VSC pour qu'il écoute chaque modifications SCSS effectuées.
      Cela compilera votre code est générera ou régénérera les fichiers dans le dossier '/css'.
